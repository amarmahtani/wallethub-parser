## Parser
### Getting Started
* The main entry point for the application is `com.ef.Parser`.
* To build the JAR file, run the following command. The Hibernate properties will need to be modified to support your database schema. See below for instructions.
```text
$ mvn clean package
```
* The jar file is available at 
```text
./target/parser.jar
```
    
### Setting Up Database Connection.
* The database configuration properties are located in `resources/hibernate.properties`.
* Modify the properties as per your database connection.
* **NOTE** For the property `hibernate.connection.url` the query parameter `serverTimezone=UTC` should NOT be deleted.
* The parser application expects the following two tables in the schema (Modify the ).
```SQL
CREATE DATABASE wallethub;

create table wallethub.IP_Log(
	/* Using a dedicated primary key ID in order to allow to track multiple parser.jar executions*/
	ID INT AUTO_INCREMENT PRIMARY KEY,
	IP_ADDRESS VARCHAR(15) NOT NULL,
	NUM_OF_REQUESTS INT NOT NULL,
	START_TIME DATETIME NOT NULL,
	END_TIME DATETIME NOT NULL,
	/* This is for audit purposes. Also can use this to lookup a specific parser.jar execution*/
	PARSE_EXECUTION_TIME DATETIME NOT NULL
);

create table wallethub.IP_Log_Detail(
	ID INT AUTO_INCREMENT PRIMARY KEY,
	IP_ID INT NOT NULL,
	REQUEST_DATE DATETIME NOT NULL,
	HTTP_METHOD VARCHAR(20) NOT NULL,
	HTTP_STATUS_CODE VARCHAR(3) NOT NULL,
	BLOCK_REASON VARCHAR(250) NOT NULL,
	FOREIGN KEY (IP_ID) REFERENCES wallethub.IP_Log(ID) ON DELETE CASCADE ON UPDATE CASCADE
);
```
### Program Flags
* The following flags are permitted,
    * `--startDate` : Indicates the report's start date. Value should be a valid date in the format `yyyy-MM-dd.HH:mm:ss` for example `2017-01-01.15:00:00`
    * `--duration` : Indicates if the report should contain an hour's worth of logs or a day's worth of logs. Allowed values are `hourly` or `daily`
    * `--threshold` : Indicates the minimum threshold of log entries made by an IP address. Value should be a positive integer.
    * `--accessLog` : Specifies the **Absolute** location of the `access.log` file. **NOTE** If this flag is not provided, the application will assume the log file is in the same folder as the `parser.jar` file.

### Executing the Program
* To execute this program first navigate to the location of the JAR file. 
* Run the `java` command. 
* The following example can be used as a reference.
```text
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200 --accessLog=/Users/amahtani/Documents/ImpDocs/Java_MySQL_Test/access.log
```

### Query Data from Database
* MySQL Query to find IPs that made more than a certain number of requests for a given time period.
```SQL
SELECT DISTINCT il.IP_ADDRESS, count(ild.ID)
FROM IP_LOG il, IP_LOG_DETAIL ild
WHERE il.ID = ild.IP_ID
AND ild.REQUEST_DATE BETWEEN '2017-01-01 15:00:06' AND '2017-01-01 15:05:08'
GROUP BY il.IP_ADDRESS
HAVING count(ild.ID) > 200;
```
* MySQL query to find requests made by a given IP
```SQL
SELECT il.IP_ADDRESS, ild.HTTP_METHOD, ild.HTTP_STATUS_CODE, ild.BLOCK_REASON 
FROM IP_LOG il, IP_LOG_DETAIL ild
WHERE il.ID = ild.IP_ID
AND il.IP_ADDRESS = '192.168.106.134'
```