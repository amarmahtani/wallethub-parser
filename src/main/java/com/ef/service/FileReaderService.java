package com.ef.service;

import com.ef.entities.IPLog;
import com.ef.entities.IPLogDetail;
import com.ef.enums.CommandLineArguments;
import com.ef.enums.Duration;
import com.ef.exceptions.LogFileException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import static com.ef.enums.CommandLineArguments.*;
import static com.ef.utils.Constants.cliDateFormat;
import static com.ef.utils.Constants.logDateFormat;

/**
 * This service reads the access.log file and populates the models IPLog and IPLogDetail
 * @author Amar Mahtani
 */
@Service("fileReaderService")
public class FileReaderService {

    private String fileName;

    private String defaultFileName;

    private Map<String, IPLog> ipAddressTologMap;

    @Autowired
    public FileReaderService(@Value("${access.log.file}") String defaultFileName) {
        this.defaultFileName= defaultFileName;
        ipAddressTologMap = new HashMap<String, IPLog>();
    }

    public List<IPLog> processLogFile(Map<CommandLineArguments, String> cliArguments) {
        this.fileName = cliArguments.get(ACCESSLOG) == null ?
                this.defaultFileName : cliArguments.get(ACCESSLOG);
        Interval interval = calculateParserInterval(cliArguments);
        DateTime parseRequestDateTime = new DateTime();
        readFile(interval, parseRequestDateTime);
        filterResults(Integer.valueOf(cliArguments.get(CommandLineArguments.THRESHOLD)));

        return new ArrayList(ipAddressTologMap.values());
    }

    private void readFile(Interval interval, DateTime parseRequestDateTime){

        FileInputStream fileInputStream = null;
        BufferedReader bufferedReader = null;
        try {
            fileInputStream = new FileInputStream(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                processLogLine(line, interval, parseRequestDateTime);
            }
            bufferedReader.close();
        } catch (IOException ex) {
            throw new LogFileException(ex.getMessage());
        } finally {
            bufferedReader = null;
        }
    }

    private Interval calculateParserInterval(Map<CommandLineArguments, String> cliArguments) {
        DateTime cliStartDateTime = null;
        DateTime cliEndDateTime = null;
        try {
            cliDateFormat.setTimeZone(TimeZone.getDefault());
            cliStartDateTime = new DateTime(cliDateFormat.parse(cliArguments.get(STARTDATE)))
                    .withZone(DateTimeZone.getDefault());
            cliEndDateTime = calculateEndDateTime(cliArguments.get(DURATION), cliStartDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Interval(cliStartDateTime, cliEndDateTime);
    }

    private void processLogLine(String line, Interval interval, DateTime parseRequestDateTime) {
        String[] logItems = line.split("\\|");

        String logDate = logItems[0];
        DateTime httpLogRequestDateTime = null;
        try {
            logDateFormat.setTimeZone(TimeZone.getDefault());
            httpLogRequestDateTime = new DateTime(logDateFormat.parse(logDate)).withZone(DateTimeZone.getDefault());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String logIpAddress = logItems[1];
        String logRequest = logItems[2];
        String logStatus = logItems[3];
        String logText = logItems[4];

        if (interval.contains(httpLogRequestDateTime)) {

            ArrayList<IPLogDetail> ipLogDetails;
            if(!ipAddressTologMap.containsKey(logIpAddress)) {

                IPLog ipLog = new IPLog(logIpAddress, 1,
                        interval.getStart().toLocalDateTime().toDate(),
                        interval.getEnd().toLocalDateTime().toDate(),
                        parseRequestDateTime.toDate(), null);

                ipLogDetails = new ArrayList<IPLogDetail>();
                ipLogDetails.add(new IPLogDetail(ipLog, httpLogRequestDateTime.toLocalDateTime().toDate(),
                        logRequest, logStatus, logText));
                ipLog.setLogDetails(ipLogDetails);

                ipAddressTologMap.put(logIpAddress, ipLog);

            } else {
                IPLog ipLog = ipAddressTologMap.get(logIpAddress);
                ipLog.getLogDetails().add(new IPLogDetail(ipLog, httpLogRequestDateTime.toLocalDateTime().toDate(),
                        logRequest, logStatus, logText));
                ipLog.setNumOfRequests(ipLog.getNumOfRequests()+1);
            }
        }
    }

    private DateTime calculateEndDateTime(String duration, DateTime startDateTime) {
        DateTime endDateTime = null;
        if(duration.equalsIgnoreCase(Duration.DAILY.toString())){
            endDateTime = startDateTime.plusDays(1);
        } else if (duration.equalsIgnoreCase(Duration.HOURLY.toString())) {
            endDateTime = startDateTime.plusHours(1);
        } else {
            endDateTime = startDateTime;
        }
        return endDateTime;
    }

    private void filterResults(Integer threshold) {
        Set<String> ipAddressKeys = ipAddressTologMap.keySet();

        // Would use streams for this approach,
        // but since I am unsure of the Java Environment, will use the FOR loop approach.

        //        ipAddressTologMap = ipAddressKeys.stream()
        //          .map(ip -> ipAddressTologMap.get(ip))
        //          .filter(log -> log.getNumOfRequests() >= threshold)
        //          .collect(Collectors.toMap(log -> log.getIpAddress(), log -> log));

        Map<String, IPLog> intermediateMap = new HashMap<String, IPLog>();
        for(String ipAddress : ipAddressKeys) {
            IPLog ipLog = ipAddressTologMap.get(ipAddress);
            if(ipLog.getNumOfRequests() >= threshold) {
                intermediateMap.put(ipAddress, ipLog);
            }
        }
        ipAddressTologMap = intermediateMap;
    }
}
