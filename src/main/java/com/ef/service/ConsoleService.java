package com.ef.service;

import com.ef.entities.IPLog;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class holds all logic to display data to the console.
 * @author Amar Mahtani
 */
@Service
public class ConsoleService {

    public void displayIpLogs(List<IPLog> ipLogs) {

        displayTopBanner();

        for(IPLog ipLog : ipLogs) {

            StringBuilder stringBuilder = new StringBuilder("\n");
            stringBuilder.append(ipLog.getIpAddress()).append("\t, ");
            stringBuilder.append("No. of Requests = ").append(ipLog.getNumOfRequests());

            System.out.println(stringBuilder.toString());
        }

        displayBottomBanner();

    }

    private void displayTopBanner() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("         PARSER RESULTS ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

    private void displayBottomBanner() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("         END OF PARSER ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

}
