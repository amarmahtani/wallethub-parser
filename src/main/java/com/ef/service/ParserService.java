package com.ef.service;

import com.ef.entities.IPLog;
import com.ef.repository.ParserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The main orchestration service that utilizes other services to parse the log file, output to console
 * and write to the database.
 * @author Amar Mahtani
 */
@Service("parserService")
public class ParserService {

    Log logger = LogFactory.getLog(ParserService.class);

    @Autowired
    CliArgumentService cliArgumentService;

    @Autowired
    FileReaderService fileReaderService;

    @Autowired
    ConsoleService consoleService;

    @Autowired
    ParserRepository parserRepository;

    /**
     * Main orchestration method
     * @param arg
     */
    public void beginService(String arg[]) {

        try {
            cliArgumentService.processCliArguments(arg);
            List<IPLog> ipLogs = fileReaderService.processLogFile(cliArgumentService.getCliArguments());
            consoleService.displayIpLogs(ipLogs);
            parserRepository.writeToDatabase(ipLogs);
        }catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }
    }

}
