package com.ef.service;

import com.ef.enums.CommandLineArguments;
import com.ef.enums.Duration;
import com.ef.exceptions.CommandLineException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static com.ef.enums.CommandLineArguments.contains;
import static com.ef.enums.CommandLineArguments.valueOf;
import static com.ef.utils.Constants.cliDateFormat;

/**
 * This class stores, validates and manages the command line arguments.
 * @author Amar Mahtani
 */
@Service("cliArgumentService")
@Scope("singleton")
public class CliArgumentService {

    private Map<CommandLineArguments, String> cliArguments;

    //We could place these regex values in a properties file, but that could leave it exposed to unwanted changes.
    private final String argumentFormatRegex = "(^[\\-]{2}[a-zA-Z]*[\\=]{1}[a-zA-Z0-9\\-\\.\\:\\/\\~\\_\\\\]*)";


    public CliArgumentService() {
        cliArguments = new HashMap<CommandLineArguments, String>();
    }

    public void processCliArguments(String[] args) {
        for(String argument : args) {

            validateArgument(argument);
            String key = getArgumentKey(argument);
            validateKey(key);
            CommandLineArguments commandLineArgument = valueOf(key.toUpperCase());
            String value = getArgumentValue(argument);
            validateValueAgainstKey(commandLineArgument, value);

            addCliArgument(commandLineArgument, value);
        }
    }

    public Map<CommandLineArguments, String> getCliArguments() {
        return cliArguments;
    }

    private void addCliArgument(CommandLineArguments commandLineArgument, String value) {
        cliArguments.put(commandLineArgument, value);
    }

    private void validateArgument(String argument) {
        if(!Pattern.matches(argumentFormatRegex, argument)) {
            throw new CommandLineException("Command line argument has incorrect format: --Key=Value");
        }
    }

    private String getArgumentKey(String argument) {
        String key = argument.split("=")[0]
                    .split("--")[1];
        return key;
    }

    private void validateKey(String key) {
        if(!contains(key)) {
            throw new CommandLineException("Invalid command line argument: Allowed values are - startDate, duration, threshold");
        }
    }

    private String getArgumentValue(String argument) {
        String value = argument.split("=")[1];
        return value;
    }

    private void validateValueAgainstKey(CommandLineArguments commandLineArgument, String value) {

        switch (commandLineArgument) {
            case DURATION :
                if(!Duration.contains(value)) {
                    throw new CommandLineException("Invalid value for Duration: Allowed values are - 'hourly', 'daily'");
                }
                break;
            case THRESHOLD:
                if(!Pattern.matches("(\\d*)", value)) {
                    throw new CommandLineException("Invalid value for Threshold: Allowed values are - any positive integer");
                }
                break;
            case STARTDATE:
                try {
                    cliDateFormat.parse(value);
                } catch (ParseException e) {
                    throw new CommandLineException("Invalid value for StartDate: Allowed values are - any valid date with the format - yyyy-MM-dd.HH:mm:ss");
                }
                break;
            case ACCESSLOG:
                File accessLogFile = new File(value);
                if(!accessLogFile.exists() || !accessLogFile.getName().equalsIgnoreCase("access.log")) {
                    throw new CommandLineException("Invalid value for AccessLog: Allowed values are - a valid path to the file access.log");
                }
                break;
        }
    }
}
