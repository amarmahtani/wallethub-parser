package com.ef.enums;

public enum CommandLineArguments {
    STARTDATE("startDate"),
    DURATION("duration"),
    THRESHOLD("threshold"),
    ACCESSLOG("accessLog");

    private final String name;

    CommandLineArguments(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static Boolean contains(String name) {
        Boolean result = false;
        for(CommandLineArguments cli : CommandLineArguments.values()) {
            if(cli.toString().equals(name)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
