package com.ef.enums;

public enum Duration {
    HOURLY,
    DAILY;

    public static Boolean contains(String name) {
        Boolean result = false;
        for(Duration duration : Duration.values()) {
            if(duration.toString().equalsIgnoreCase(name)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
