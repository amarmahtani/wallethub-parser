package com.ef.utils;

import java.text.SimpleDateFormat;

public interface Constants {

    public static final SimpleDateFormat logDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static final SimpleDateFormat cliDateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
}
