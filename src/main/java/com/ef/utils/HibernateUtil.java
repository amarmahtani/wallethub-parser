package com.ef.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.IOException;
import java.util.Properties;

/**
 * Utility class to handle Hibernate session factory.
 * @author Amar Mahtani
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    static {
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(HibernateUtil.class.getClassLoader().getResourceAsStream("hibernate.properties"));
        } catch (IOException e) {
        }
        Configuration configuration = new Configuration().mergeProperties(dbProperties).configure();
        sessionFactory = configuration.buildSessionFactory();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
