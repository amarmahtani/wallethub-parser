package com.ef.repository;

import com.ef.entities.IPLog;
import com.ef.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.TimeZone;

/**
 * This class is the Data Access Class that writes the IPLog and IPLogDetails to the database.
 * @author Amar Mahtani
 */
@Repository
public class ParserRepository {

    SessionFactory sessionFactory = null;
    Session session = null;

    public void writeToDatabase(List<IPLog> ipLogs) {

        session = getSession();

        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            saveIpLogs(ipLogs);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        } finally {
            session.close();
            sessionFactory.close();
        }

    }

    private void saveIpLogs(List<IPLog> ipLogs) {
        for(IPLog ipLog : ipLogs) {
            session.save(ipLog);
        }
    }

    private Session getSession() {
        sessionFactory = HibernateUtil.getSessionFactory();
        Session currentSession = sessionFactory
                .withOptions()
                .jdbcTimeZone(TimeZone.getDefault())
                .openSession();
        return currentSession;
    }
}
