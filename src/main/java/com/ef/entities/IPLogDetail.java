package com.ef.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "IP_LOG_DETAIL")
public class IPLogDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @ManyToOne
    @JoinColumn(name="IP_ID", nullable = true)
    private IPLog ipLog;

    @Column(name = "REQUEST_DATE")
    private Date requestDate;

    @Column(name = "HTTP_METHOD")
    private String httpMethod;

    @Column(name = "HTTP_STATUS_CODE")
    private String httpStatusCode;

    @Column(name = "BLOCK_REASON")
    private String blockReason;

    public IPLogDetail() {
    }

    public IPLogDetail(IPLog ipLog, Date requestDate, String httpMethod, String httpStatusCode, String blockReason) {
        this.ipLog = ipLog;
        this.requestDate = requestDate;
        this.httpMethod = httpMethod;
        this.httpStatusCode = httpStatusCode;
        this.blockReason = blockReason;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public IPLog getIpLog() {
        return ipLog;
    }

    public void setIpLog(IPLog ipLog) {
        this.ipLog = ipLog;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getBlockReason() {
        return blockReason;
    }

    public void setBlockReason(String blockReason) {
        this.blockReason = blockReason;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("\n");
        sb.append(ipLog.getIpAddress()).append("\\|");
        sb.append(requestDate).append("\\|");
        sb.append(httpMethod).append("\\|");
        sb.append(httpStatusCode).append("\\|");
        sb.append(blockReason).append("\n");
        return sb.toString();
    }
}
