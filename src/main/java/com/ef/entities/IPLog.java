package com.ef.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="IP_LOG")
public class IPLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "IP_ADDRESS")
    private String ipAddress;

    @Column(name = "NUM_OF_REQUESTS")
    private int numOfRequests;

    @Column(name = "START_TIME")
    private Date startTime;

    @Column(name = "END_TIME")
    private Date endTime;

    @Column(name = "PARSE_EXECUTION_TIME")
    private Date parseExecutionTime;

    @OneToMany(mappedBy = "ipLog", cascade = CascadeType.ALL)
    private List<IPLogDetail> logDetails;

    public IPLog() {
    }

    public IPLog(String ipAddress, int numOfRequests, Date startTime, Date endTime, Date parseExecutionTime, List<IPLogDetail> logDetails) {
        this.ipAddress = ipAddress;
        this.numOfRequests = numOfRequests;
        this.startTime = startTime;
        this.endTime = endTime;
        this.parseExecutionTime = parseExecutionTime;
        this.logDetails = logDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getNumOfRequests() {
        return numOfRequests;
    }

    public void setNumOfRequests(int numOfRequests) {
        this.numOfRequests = numOfRequests;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getParseExecutionTime() {
        return parseExecutionTime;
    }

    public void setParseExecutionTime(Date parseExecutionTime) {
        this.parseExecutionTime = parseExecutionTime;
    }

    public List<IPLogDetail> getLogDetails() {
        return logDetails;
    }

    public void setLogDetails(List<IPLogDetail> logDetails) {
        this.logDetails = logDetails;
    }

    @Override
    public boolean equals(Object obj) {
        IPLog ipLog = (IPLog) obj;
        return this.ipAddress.equalsIgnoreCase(ipLog.ipAddress);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("\n");
        sb.append(ipAddress).append(" | ");
        sb.append("numOfRequests=").append(numOfRequests);
        sb.append(" between ").append(startTime);
        sb.append(" and ").append(endTime);
        sb.append("\n").append(logDetails);
        return sb.toString();
    }
}
