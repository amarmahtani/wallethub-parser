package com.ef.exceptions;

/**
 * Business exception class related to Log File operations.
 * @author  Amar Mahtani
 */
public class LogFileException extends RuntimeException {

    public LogFileException(String message) {
        super(message);
    }
}
