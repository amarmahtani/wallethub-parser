package com.ef.exceptions;

/**
 * Business exception class related to Command Line Argument operations.
 * @author  Amar Mahtani
 */
public class CommandLineException extends RuntimeException {
    public CommandLineException(String exMessage) {
        super(exMessage);
    }
}
