package com.ef;

import com.ef.service.ParserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Entry point to the application.
 * @author Amar Mahtani
 */
public class Parser {

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new ClassPathXmlApplicationContext("applicationContext.xml");

        ParserService parserService = applicationContext.getBean("parserService", ParserService.class);

        parserService.beginService(args);

    }

}
